# suite_ToDoApp_robotframework

## Instalação

Robot Framework requer [Pyton](https://nodejs.org/) v2.7+ to run.

Instale as dependências.

```sh
pip install robotframework
pip install --upgrade robotframework-seleniumlibrary
pip install --upgrade robotframework-screencaplibrary
pip install --upgrade robotframework-sshlibrary
pip install --upgrade RESTinstance
pip install robotframework-requests
```

## Extensões

Vamos instalar as extenções que irão nos auxiliar na criação dos testes  

| Extensão |Download|
| ------ | ------ |
| Robot Framework Intellisense | https://marketplace.visualstudio.com/items?itemName=TomiTurtiainen.rf-intellisense |
| Material Icon Theme (Opcional) | https://marketplace.visualstudio.com/items?itemName=PKief.material-icon-theme |
| Dracula Officia (Opcional) | https://marketplace.visualstudio.com/items?itemName=dracula-theme.theme-dracula |
| GitLens — Git supercharged (Opcional) | https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens |

## Uso
### Execução de um cenário de teste específico
```sh
robot -d ./MeusResultados tests/Gerência_de_Falhas/Alarmes/UC001.robot
```
### Execução de um teste específico
```sh
robot -d ./MeusResultados -t "T-BDEH-25 : Fluxo Básico - Visualizar Alarmes" tests
```
### Fluxos básicos
```sh
robot -d ./MeusResultados -i FluxoBásico tests
```
### Fluxos Alternativos
```sh
robot -d ./MeusResultados -i FluxoAlternativo tests
```
### Fluxos de exceção
```sh
robot -d ./MeusResultados -i FluxoExceção tests
```
### Testes por modulo
* Ações
```sh
robot -d ./MeusResultados -i FluxoAções tests
```
* Agrupamentos
```sh
robot -d ./MeusResultados -i FluxoAgrupamentos tests
```
* Alarmes
```sh
robot -d ./MeusResultados -i FluxoAlarmes tests
```
* Eventos
```sh
robot -d ./MeusResultados -i FluxoEventos tests
```
* Dashboard
```sh
robot -d ./MeusResultados -i FluxoDashboard tests
```
* Relatórios
```sh
robot -d ./MeusResultados -i FluxoRelatório tests
```

### _Como funciona os comandos de Execução do Robot Framework?_

  - **Executar apenas um teste específico da suíte:**
```robot -d ./MeusResultados -t “Nome do Cenário” Testes```
Onde, -t é o parâmetro que indica que deve ser executado o(s) teste(s) específico(s) chamado(s) (“Nome do Cenário”) das suítes contidas no diretório (Testes). OBS.: Se existirem testes em diferentes suítes, mas com esse mesmo nome, todos serão executados.
  - **Executando por TAGS:**
```robot -d ./MeusResultados -i (Nome da Tag sem parênteses) Testes```
Assim, posso rodar comandos solicitando que sejam executados somente testes identificados com determinada Tag, onde, -i é o parâmetro que indica que devem ser executados apenas os testes cuja a Tag seja (Nome da Tag).
  - **Atribuindo valores a variáveis:**
```robot -d ./MeusResultados -v BROWSER:chrome Testes```
Onde, -v é o parâmetro que indica que será atribuído o valor (chrome) à variável (${BROWSER}) do meu teste. No meu exemplo, a variável está definida como default o valor firefox, mas rodando por este comando, o navegador utilizado será o chrome.

Esses são alguns comandos básicos possíveis de execução. Para saber outras possibilidades e comandos mais avançadas, consulte:
[RobotFrameworkUserGuide](http://robotframework.org/robotframework/latest/RobotFrameworkUserGuide.html#configuring-execution)

# Relatórios
Especificar o diretório dos results da execução
```sh
robot -d ./MeusResultados Testes
```
Onde, -d é o parâmetro que indica que os resultados da execução (log/output/report) das suítes contidas no diretório (Testes) devem ser armazenados em um diretório específico, que no exemplo é uma pasta de nível acima (./), chamada (MeusResultados).
Obs: Se não indicarmos um diretório para os relatórios eles serão salvos na raíz do projeto

