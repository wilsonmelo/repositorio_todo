*** Settings ***
Documentation     Setup de keywords para os casos de teste da pagina MyTasks
Library           SeleniumLibrary     timeout=10 
Resource          ..${/}../resources/model/variables.robot


*** Keywords ***

Ir para a tela MyTasks
    Go To  ${URL}/tasks

Verificar mensagem no topo da tela
    Wait Until Element Is Visible  xpath=/html/body/div[1]/h1
    Element Text Should Be  xpath=/html/body/div[1]/h1  Hey John, this is your todo list for today:

Preencher descrição com 2 caracteres
    Wait Until Element Is Visible  name=new_task
    Input Text       name=new_task     nt

Preencher descrição com 3 caracteres
    Wait Until Element Is Visible  name=new_task
    Input Text       name=new_task     nta

Preencher descrição com 250 caracteres
    Wait Until Element Is Visible  name=new_task
    Input Text       name=new_task     novatasknovatasknovatasknovatasknovatasknovatasknovatasknovatasknovatasknovatasknovatasknovatasknovatasknovatasknovatasknovatasknovatasknovatasknovatasknovatasknovatasknovatasknovatasknovatasknovatasknovatasknovatasknovatasknovatasknovatasknovataskno
    
Preencher descrição com 251 caracteres
    Wait Until Element Is Visible  name=new_task
    Input Text       name=new_task     novatasknovatasknovatasknovatasknovatasknovatasknovatasknovatasknovatasknovatasknovatasknovatasknovatasknovatasknovatasknovatasknovatasknovatasknovatasknovatasknovatasknovatasknovatasknovatasknovatasknovatasknovatasknovatasknovatasknovatasknovatasknov
    
Tentar inserir task e verificar se sistema permitiu
    Sleep  1s
    ${numero_registros_tabela_pre_insercao} =	get element count	xpath:/html/body/div[1]/div[2]/div[2]/div[2]/table/tbody/tr
    Log to Console   ${numero_registros_tabela_pre_insercao}
    Press Keys           None  RETURN
    Sleep  1s
    ${numero_registros_tabela_pos_insercao} =   get element count  xpath:/html/body/div[1]/div[2]/div[2]/div[2]/table/tbody/tr
    Log to Console   ${numero_registros_tabela_pos_insercao}
    Should Be Equal  	"${numero_registros_tabela_pre_insercao}"  "${numero_registros_tabela_pos_insercao}"

Tentar inserir task e verificar que sistema permitiu
    Sleep  1s
    ${numero_registros_tabela_pre_insercao}=  get element count  xpath:/html/body/div[1]/div[2]/div[2]/div[2]/table/tbody/tr
    Log to Console   ${numero_registros_tabela_pre_insercao}
    Press Keys           None  RETURN
    Sleep  1s
    ${numero_registros_tabela_pos_insercao}=  get element count	 xpath:/html/body/div[1]/div[2]/div[2]/div[2]/table/tbody/tr
    Log to Console   ${numero_registros_tabela_pos_insercao}
    Should Be True   ${numero_registros_tabela_pos_insercao} > ${numero_registros_tabela_pre_insercao}

Confirmar inserção 
    Press Keys     None  RETURN

Clicar na opção 'remove'
    Wait Until Element Is Visible  xpath=/html/body/div[1]/div[2]/div[2]/div[2]/table/tbody/tr/td[5]/button
    Click Element   xpath=/html/body/div[1]/div[2]/div[2]/div[2]/table/tbody/tr/td[5]/button

Clicar na opção 'Manage Subtask'
    Wait Until Element Is Visible  xpath=/html/body/div[1]/div[2]/div[2]/div[2]/table/tbody/tr/td[4]/button
    Click Element   xpath=/html/body/div[1]/div[2]/div[2]/div[2]/table/tbody/tr/td[4]/button

Tentar inserir subtask sem preencher campos obrigatórios e verificar se sistema permitiu
    Input Text       name=due_date  12/01/2021
    Press Keys       None  RETURN
    Sleep  3s
    ${numero_registros_tabela_pos_insercao} =  Get Element Count	xpath:/html/body/div[4]/div/div/div[2]/div[2]/table/tbody/tr
    Log to Console    ${numero_registros_tabela_pos_insercao}
    Should Be Equal As Strings 	"${numero_registros_tabela_pos_insercao}"  "0"  

