*** Settings ***
Documentation     Presetup
Library           SeleniumLibrary    timeout=10
Library           REST
Resource          variables.robot

*** Variables ***
${BROWSER}    chrome
${AccessToken}

*** Keywords ***
Login no ToDo App 
    Open Browser    ${URL}/users/sign_in   ${BROWSER}
    Maximize browser window
    Wait Until Element Is Visible   //input[@name="user[email]"]   
    Input Text    //input[@name="user[email]"]    wilsonmelo2@hotmail.com
    Wait Until Element Is Visible  //input[@name="user[password]"]
    Input Text    //input[@name="user[password]"]    Provisoria@2020
    Click Element    //input[@name="commit"]
    
Fechar navegador
    Close Browser
    
Adicionar task
###Forma de cadastro via API
    REST.Post  ${ENDPOINT_INSERT_TASK}  body=${Task_Json}  headers={"Content-Type": "application/json"}
###Forma de cadastro via FrontEnd
    Ir para a tela MyTasks
    Preencher descrição com 3 caracteres
    Confirmar inserção 
    Sleep  1s

Remover task
    Clicar na opção 'remove'
    Sleep  1s






    


