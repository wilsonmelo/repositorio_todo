*** Settings ***

*** Variables ***

### URL ToDo App
${URL}    https://qa-test.avenuecode.com

### Endpoint cadastro de task
${ENDPOINT_INSERT_TASK}    https://qa-test.avenuecode.com/tasks.json

### Acessos
${INPUT_USER}        id:user_email
${INPUT_PASSWORD}    id:user_password 

### Task automzatizada Json
${Task_Json}     {"body": "teste", "public": true}

### Variáveis utilizadas para verificar se o número de elementos da tabela aumentou, ou seja, 
### Se foi possível inserir o objeto
###-------------------------------------
### Outra opção seria verificar as mensagens do tipo "o cadastro não foi possível"
### No entanto, o ToDo App não possui tais mensagens em sua atual versão
${numero_registros_tabela_pre_insercao} 
${numero_registros_tabela_pos_insercao}



