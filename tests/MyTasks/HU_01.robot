*** Settings ***
Documentation     HU01 - Adicionar task 
Resource          ..${/}../resources/model/presetup.robot
Resource          ..${/}../resources/keywords/K_MyTasks.robot

Suite Setup     Login no ToDo App 
Test Setup      
Suite Teardown  Fechar navegador
Test Teardown   

*** Test Cases ***
Cenário-01 : Visualizar mensagem no topo da tela
    [Tags]  FluxoBásico
    ...     FluxoMyTasks
    Ir para a tela MyTasks
    Verificar mensagem no topo da tela

#------------------------------------------------------------
# Para os testes referentes aos limites do número de caracteres de descrição das tasks
# aplicamos a técnica de análise do valor limite e partição de equivalência. Analisamos nos limites 
# inferiores e superiores, assim como, os intervalos válido e inválido.
# O mesmo raciocínio aplicado para tasks pode ser aplicado para validar os limites para subtasks

# Deve falhar já que hoje o sistema apresenta esse incidente
Cenário-02 : Inserir task com descrição contendo 2 caracteres
    [Tags]  FluxoBásico
    ...     FluxoMyTasks
    Ir para a tela MyTasks
    Preencher descrição com 2 caracteres
    Tentar inserir task e verificar se sistema permitiu
    [Teardown]  Remover task
    
 # Deve passar pois condiz com os critérios de aceitação
Cenário-03 : Inserir task com descrição contendo 3 caracteres
   [Tags]  FluxoBásico
   ...     FluxoMyTasks
    Ir para a tela MyTasks
    Preencher descrição com 3 caracteres
    Tentar inserir task e verificar que sistema permitiu
    [Teardown]  Remover task

# Deve passar pois condiz com os critérios de aceitação
Cenário-04 : Inserir task com descrição contendo 250 caracteres
    [Tags]  FluxoBásico
    ...     FluxoMyTasks
    Ir para a tela MyTasks
    Preencher descrição com 250 caracteres
    Tentar inserir task e verificar que sistema permitiu
    [Teardown]  Remover task

# Deve falhar já que hoje o sistema apresenta esse incidente
Cenário-05 : Inserir task com descrição contendo 251 caracteres
    [Tags]  FluxoBásico
    ...     FluxoMyTasks
    Ir para a tela MyTasks
    Preencher descrição com 251 caracteres
    Tentar inserir task e verificar se sistema permitiu
    [Teardown]  Remover task

#------------------------------------------------
# Deve falhar já que hoje o sistema apresenta esse incidente. Aqui fizemos para o campo descrição, mas o mesmo 
# raciocinio deve ser aplicado para testar o outro campo obrigatório
Cenário-06 : Criar subtask sem descrição
    [Tags]  FluxoBásico
    ...     FluxoMyTasks
    # Para esse cenário há a pré-condição de que deve haver pelo menos uma task cadastrada 
    # no sistema por isso, antes de iniciar os passos, chamamos o [setup] Adicionar task. 
    # Além disso, no fim do caso de teste chamamos o [teardown] remover task, para que a 
    # task cadastrada não influencia nos testes que subsequentes

    [Setup]  Adicionar task
    Ir para a tela MyTasks
    Clicar na opção 'Manage Subtask'
    Tentar inserir subtask sem preencher campos obrigatórios e verificar se sistema permitiu
    Ir para a tela MyTasks
    [Teardown]  Remover task